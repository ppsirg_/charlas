# charla db no sql - redis-mongodb

pass: dev2016

## introduccion

recientemente estan "de moda" las bases de datos no relacionales, a diferencia de las
bases de datos sql, las no relacionales se basan principalmente en el concepto de llave
valor

### bases de datos no-relacionales: para que fueron creadas?

* velocidad
* flexibilidad

#### velocidad para procesos asincronos

el uso de multiples núcleos y mejores formas de almacenamiento de datos ha presionado
al diseño de software a aprovechar de mejor manera estas capacidades, en este sentido
esta surgiendo la tendencia en el desarrollo a nivel de servidor de desarrollar hacia
el paradigma de la asincronicidad.

a diferencia de los servidores basados en multiples hilos, los servidores de un solo
hilo-un solo proceso basan su funcionamiento en el manejo de la ejecución de las tareas
haciendo que se ejecuten ajenas al servidor (como nodejs) o con tecnicas de interrupcion
de ejecución (como el context manager de tornado), estas tecnicas han logrado que los
servidores puedan pasar de manejar miles de peticiones por segundo a millones de
peticiones por segundo

con este volumen de transacciones, el tiempo de la consulta de la base de datos es critico,
hay soluciones como la gestion asincrona de multiples bases de datos(como lo hace momoko),
streaming de datos(como lo hace postgres), pero los tiempos por request de una base de datos
llegan a un limite que no puede bajarse eventualmente

el hecho de que las db no-sql se basen en el paradigma de llave-valor hace que las consultas
sean mucho más rápidas, llegando a obtenerse diferencias hasta de diez veces entre bases de
datos sql y bases de datos no-sql

#### flexibilidad de negocio

* las bases de datos sql necesitan de un esquema, el diseño de este es muchas veces uno de los
puntos más criticos del desarrollo, hay situaciones en las que por la logica de negocio
los datos son muy cambiantes
* durante el desarrollo es util montar dummies de un api para poder ayudar a que la parte de
frontend realice pruebas sin necesidad de esperar el codigo completo del backend

#### intercomunicar diferentes partes de un sistema

en el esfuerzo por modularizar y escalar sistemas complejos se usan sistemas de bases de datos
rápidos como redis para que sirvan de puentes, usualmente entre procesos de diferentes
tecnologías como puede ser django y nodejs o entre instancias de una misma aplicación que corren
en diferentes servidores

### preceptos para diseñar con bases de datos no-relacionales

usarlas si:

* se necesita mucha velocidad
* no hay muchas relaciones entre los registros que se almacenan
* no es necesaria una logica muy compleja de peticiones
* se usa para una circunstancia especifica (como puente de comunicacion o cache adicional o como un dummy)
* cuando los datos no se puedan modelar eficientemente de la manera tradicional

cuando se usan, considerar:

* tipo de datos a guardar
* tipo de consultas a realizar
* uso de la base de datos

## redis: exploración y caso practico:


### tipos de datos

los tipos de datos que maneja redis son:

* string
* lista
* hashes
* conjuntos (sets)
* conjuntos ordenados

la documentación completa se encuentra [aqui](http://redis.io/topics/data-types-intro)

### uso desde consola

en la consola se usa redis a través del comando __redis-cli__

* mirar si el server funciona bien ```ping```
* guardar un dato
    * string: ```set llave valor``` ```mset llave 1 valor1 llave2 valor2```
    * lista: ```lpush lista valor``` ```rpush lista valor```
    * hashes: ```hset registro llave_registro valor_registro```
    * conjuntos: ```sadd conjunto valor1 valor2 valor3```
* recuperar el dato
    * string: ```get llave valor```
    * lista: ```rpop lista```
    * hash: ```hget registro llave_registro``` ```hget registro```
    * conjuntos: ```spop conjunto```
* borrar dato ```DEL llave```
* escribir datos a disco
    * sincrono ```SAVE```
    * asincrono ```BGSAVE```
* limpiar base de datos ```FLUSHDB```
* limpiar todo ```FLUSHALL```

### uso desde lenguaje de programación (python)

binders para el lenguaje [redis-py](https://redis-py.readthedocs.org/en/latest/) para python

```
#!python

import redis

#crear conexion
r = redis.StrictRedis(host='localhost', port=6379, db=0)

#revisar si un registro existe, get retorna None si no existe
if not r.get('total_registers'):
    #crear un registro
    r.set('total_registers', 0)
#hace una consulta de un valor e inmediatamente lo altera con otro
r.getset('total_registers', 1)
#busca en redis con una expresion regular
r.keys(pattern='total_[.]*')
```

funciones interesantes de redis:

* dbsize() number of registers
* expireat(name, when) expire a key in a certain time, python datetime or unix time
* flushall() delete all keys in current host
* flushdb() delete all keys in current db
* info() shows info of the redis server
* keys(pattern='') return a dictionary with all keys that match
* lastsave() return last time saved to disk
* lpush(name, \*values) push item in the head of a list
* ??setrange(name, offset, value)
* get(name) get a variable by name
* getset(name, value) get a variable and save there a value
* set(name, value) set a value in a register named by name

### guardado de datos y consultas

las consultas se pueden hacer basicamente de dos tipos:

* expresiones regulares a los nombres de las llaves
* por los rangos en el caso de los conjuntos ordenados

### copias de seguridad - restauración de datos

cuando usamos SAVE o BGSAVE hacemos un archivo de los datos en redis, para cargar uno de
estos archivos solamente es necesario reemplazar el archivo y re-iniciar redis

[mas informacion en esta guia](http://redis4you.com/articles.php?id=010)
