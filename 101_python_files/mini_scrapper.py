from bs4 import BeautifulSoup
from urllib.request import urlretrieve, urlopen
import threading
import time
import requests


class publication():
    """defines a publication, given the url gets all info required to do
    social network publication of a newsbtc new or event. Also can build
    any required custom-build publication on social networks by organize
    the required information"""
    title = ''
    url = ''
    image_link = ''

    def __init__(self, url, aditional='', mode='news', **kwargs):
        """class builder, always requires a url of the article, event or
        a custom valid url, also can require keyword arguments like
        aditional for aditional plain text to include in publication,
        mode that can be 'news' for a valid newsbtc url, 'event' for a valid
        newsbtc event url and 'custom' for custom-build publications,
        or data that recieves a dictionary used for custom-build publications,
        data must contain text, image(given in a url) and a description.

        This class define all data required to publish on
        facebook and twitter"""
        self.url = url
        #aditional publication info
        self.aditional = aditional
        #get the webpage
        try:
            text = urlopen(self.url, timeout=10).read()
        except:
            text = requests.get(self.url).text
        #parse html with default parser
        soup = BeautifulSoup(text, "html.parser")
        if mode in ['news', 'event']:
            #get post name
            self.title = str(soup.find('h1', class_='entry-title').string)
            if mode == 'event':
                self.title = '#eventNewsbtcLTA ' + self.title
            #get image url
            cl = 'attachment-post-thumbnail wp-post-image'
            img = soup.find_all('img', class_=cl)
            self.image_url = img[0]['src']
            #get image
            img_name, headers = urlretrieve(self.image_url)
            self.image_link = img_name
            #get description
            desc = soup.find_all('meta', property='og:description')
            self.description = desc[0]['content']
            #get author
            try:
                self.author = str(soup.find_all('h3',
                    class_='author-title')[0].string)
            except:
                self.author = str(soup.find_all('a',
                    class_='url fn n')[0].string)
        elif mode == 'custom':
            data = kwargs['data']
            #get post name
            self.title = data['text']
            #get image url
            self.image_url = data['image']
            #get image
            img_name, headers = urlretrieve(self.image_url)
            self.image_link = img_name
            #get description
            self.description = data['description']
            #get author
            self.author = 'NewsBTC LTA'

    def getData(self):
        """get data of publication in a dictionary, keywords are:
            title, url, image_link, image_url, description and author"""
        return {'title': self.title,
                'url': self.url,
                'image_link': self.image_link,
                'image_url': self.image_url,
                'description': self.description,
                'author': self.author}


if __name__ == '__main__':
    p = publication(
        'http://lta.newsbtc.com/2015/12/17/colombianos-hora-de-ahorrar-en-bitcoins/'
        )
    data = p.getData()
    print(data)
