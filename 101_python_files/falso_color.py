# - @ Autor: Carlos Campo - #

import numpy as np
from cv2 import *


cap = VideoCapture(0)

TablaV = np.linspace(1, 255, 255)
TablaR = np.zeros(256)
TablaG = np.zeros(256)
TablaB = np.zeros(256)

recta = np.uint8(3 * TablaV[0: 84])

TablaR[0: 84] = recta
TablaR[85: 255] = 0

TablaG[0: 84] = 0
TablaG[85: 169] = recta
TablaG[170: 254] = 0

TablaB[0: 169] = 0
TablaB[170: 254] = recta


while (True):
    next, frame = cap.read()

    gray = cvtColor(frame, COLOR_BGR2GRAY)
    ima = cvtColor(frame, 1)

    imaR = TablaR[gray - 1]
    imaG = TablaG[gray - 1]
    imaB = TablaB[gray - 1]

    ima[:, :, 0] = imaB
    ima[:, :, 1] = imaG
    ima[:, :, 2] = imaR

    imshow("WebCam", ima)
    if waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
destroyAllWindows()
