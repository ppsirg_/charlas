# USO GIT - FLUJOS DE TRABAJO CON GIT

pass: dev2016

# SESION 1

## 1. INTRODUCCION

hace un tiempo se discutia cual era la joya de la corona de los trabajos de linus
torvalds, si linux o git, y es que hoy muchos proyectos y compañías deben mucho
a git, uno de los sistemas de gestion de versiones y flujos de trabajo más populares
entre los desarrolladores de software

en esta charla vamos a ver un poco sobre git, brevemente como se instala, como se
configura, un poco sobre el trabajo con plataformas como bitbucket o github y
principalmente, una orientacion a las tareas comunes a la hora de usar git, de la
misma forma que la forma de usarlo efectivamente para gestionar nuestros
proyectos de software y trabajar en equipo a través de los flujos de trabajo
de git

## 2. USO DE GIT

para empezar a usar git, es necesario instalarlo, configurarlo y entender como
funciona para poder empezar a guardar cambios y versiones de nuestro código

### 2.1. INSTALAR GIT

es tan sencillo en linux como escribir ```sudo apt-get install git``` o instalar en cualquier sistema operativo programas como:

* [git kraken (cross platform)](http://www.gitkraken.com/)
* [source tree (win, mac)](https://www.sourcetreeapp.com/)
* [git for windows](https://git-scm.com/download/win)


### 2.2. CONFIGURACION INICIAL

hay que configurar principalmente nuestra identidad, la cual consiste de nuestro
nombre, nuestro correo y una llave criptografica (no es obligatoria pero si muy
util tenerla), esto lo hacemos con las siguientes líneas:

```
git config --global user.name 'tu nombre'
git config --global user.email 'tu@email.com'
```

para el tema de configurar llaves, recomendamos seguir la [guia de altasian de configuracion de git](https://confluence.atlassian.com/bitbucket/configure-multiple-ssh-identities-for-gitbash-mac-osx-linux-271943168.html)

### 2.3. CUENTAS SERVIDORES REMOTOS

hay servicios como bitbucket o github que son páginas donde podemos tener gratis
repositorios bajo ciertas condiciones:

* github: cualquier cantidad de repositorios públicos gratis, costo mensual por
repositorios privados, es usado mucho para proyectos opensource por este motivo
* bitbucket: cualquier cantidad de repositorios privados y públicos gratis,
costo mensual por repositorios compartidos (en equipos de desarrolladores) para
un grupo mayor a 6 personas

en estos sitios te creas una cuenta, creas tu repositorio y este funcionará como
un repositorio remoto (una carpeta remota donde puedes subir tu información fiel
copia de la que tienes en tu equipo).

además de este servicio, suelen ofrecer otros similare como el issue tracker o
rastreador de incidencias, un gestor de proyectos en el caso de bitbucket o gits
el caso de github

otra funcion muy positiva de estos sitios es que te permiten administrar diferentes
tipos de llaves criptograficas para controlar el acceso de tu información entre
multiples equipos y fuentes, pero además te permite gestionar los despliegues de
las aplicaciónes y tener un control de descargas de tu proyecto

### 2.4. COMO USAR GIT

para usar git, hay que tener en cuenta como funciona, se centra en el repositorio,
un repositorio es una carpeta la cual git puede vigilar y dentro de la cual vamos
a poner nuestros archivos.

cuando cambiamos un archivo, git puede saber que se han hecho modifcaciones, y
usando ```git status``` nos muestra las mismas.


### 2.5. TAREAS COMUNES

las tareas mas comunes para las que usamos git son:

#### añadir cambios

nosotros como usuarios podemos elegir cuales archivos queremos que git tenga en
cuenta para "seguile el rastro", lo cual git entenderá a través del comando ```git add archivos_a_seguir``` donde "archivos_a_seguir" puede ser especificado
de diferentes maneras que se enunciaran adelante.

#### guardar cambios

una vez git sepa que archivos va a tener en cuenta para seguir (y cuales a ignorar
[ver .gitignore](https://git-scm.com/docs/gitignore)[1](https://github.com/github/gitignore))
se procede a registrar estos con ```git commit -m 'descripcion de los cambios que se hicieron'```

#### etiquetar entregas

al usar ```git commit``` tenemos una especie de instantanea, una version del codigo
que estamos trabajando, sin embargo los id de los commits suelen ser numeros
hexadecimales dificiles de entender, por lo cual una persona puede asignar una
etiqueta o tag más agradable, lo cual se puede hacer con el comando ```git tag etiqueta``` donde etiqueta puede ser algo como la version del programa
o algo que nos ayude a saber que hay en esa version de código (como puede ser un
bugfix)

#### agregar un servidor remoto

para cuando tenemos una cuenta de github o bitbucket y queremos subir nuestro
codigo y respaldarlo ahi, lo primero que hacemos es agregar la direccion
de ese repositorio remoto, para eso usamos:

```
git remote add nombre\_del\_remoto url\_del\_remoto
```

#### subir cambios a servidor remoto

cuando tenemos nuestro servidor remoto configurado como se explico en el paso
anterioro, podemos enviar nuestro trabajo con el comando ```git push```

#### cambiar entre diferentes versiones de un proyecto

cuando queremos cambiar entre diferentes versiones de un proyecto podemos usar
el comando ```git checkout```

# SESION 2

#### crear ramas de trabajo (branch)

una rama es una bifurcacion del codigo que se hace para poder trabajar partiendo
desde una version sin interferir con el código base en si, sino con una copia
que se va alterando de manera diferente sin dejar huella en el código original

las ramas tienen los siguientes aspectos:

* la rama padre: es de donde nace la rama, la rama por defecto siempre se llama master. la rama padre en git va a ser la rama en la que nos encontremos a la hora de crear una nueva rama, es importante reconocerla ya que el codigo inicial que tengamos en la nueva rama es exactamente el mismo de la rama padre al momento de crear la rama
* nombre de la rama: es el nombre para saber como se llama la rama, nos permite acceder a ella y cambiar entre ramas

respecto a las ramas, las dos tareas mas comunes son:

* crear rama, para ello se escribe ```git checkout -b nombre_de_mi_rama```, noten que el comando es el mismo para pasar de una tag a otra, ```git checkout```, pero el ```-b``` es el que le dice a git checkout que queremos pasarnos a una rama y que encima. la cree nueva a partir de la rama actual
* para pasarse entre ramas, se usa ```git checkout nombre_de_rama_destino```, exactamente igual que cuando cambiamos entre etiquetas

## 3. PLANEACION DE UN PROYECTO DE SOFTWARE

para el uso eficiente del trabajo en equipo entre varios miembros la planeacion de
proyecto debe hacerse de tal manera que se prime la capacidad de que multiples miembros del
equipo puedan trabajar en paralelo en diferentes funcionalidades del mismo que se comuniquen
entre ellas a través de interfaces definidas de comun acuerdo desde el inicio.

un ejemplo de lo anterior es el diseño de aplicaciónes web cuando estas se orientan al consumo
de servicios, es común que en estos proyectos se encuentre un desarrollador de backend y uno
de frontend, al inicio se planea los recursos que el segundo va a usar para que el primero los
haga en servidor y el segundo los simule en el cliente durante el desarrollo, un ejemplo es:

aplicacion: mostrar recetas de tragos

interfaz(api-rest):

* ver lista de recetas:
    * url:  ```/recetas```, metodo ```GET```
    * body: enviado a server ```""```, recibido de server ```'[id1,id2,id3,id4,id5,id6,id7,id8]'```
* crear nueva receta:
    * url:  ```/recetas```, metodo ```POST```
    * body: enviado a server ```{'nombre': 'mojito', 'ingredientes': 'ron, ginger, yerbabuena...'}```, recibido de server ```'{'id':newid, 'status': 'created'}'```
* ver detalles de una receta con su id:
    * url:  ```/recetas/id_receta```, metodo ```GET```
    * body: enviado a server ```""```, recibido de server ```'{'id': id_receta,'nombre': 'mojito', 'ingredientes': 'ron, ginger, yerbabuena...'}'```

en este caso tanto backend como frontend pueden trabajar en paralelo e integrar al final, lo cual
significa que cada uno puede trabajar en un branch o rama diferente sin que su código interfiera
con el de su compañero y con un bajo riesgo de conflictos al realizar la integracion de ramas

### 3.1 POO APLICADO A PROYECTOS: PLANEACION POR CARACTERISTICAS

una manera de planear proyecto es la del PMP (project management institute) la
cual enseña a dividir los proyectos en entregables (objetos, bien sean materiales
o virtuales que puedan constatarse y/o medirse), para lograr lo anterior (trabajo en paralelo)
la idea es hacer que esos entregables partan de un codigo base, tests automatizados y documentacion minimos
y que puedan funcionar por separado, y que al final se entreguen en un formato similar a las librerias
para que quien realiza el trabajo de integración pueda ensamblar todo al final.

en este caso, la idea es planear el desarrollo de tal manera que inicialmente un archivo solo sea trabajado
por una sola persona en un branch

#### arbol de proyecto

arbol de proyecto es la planeación de la estructura de archivos, carpetas y branches de un proyecto

#### documentacion inicial

**ES VITAL UNA BUENA DOCUMENTACION INICIAL, PORQUE DECLARA AL EQUIPO QUE ES LO QUE SE VA A HACER Y CUAL ES LA
_IDEA_ DEL DESARROLLO**

la documentacion inicial debe tener un resumen de lo que se quiere, de lo que se va a usar, de las caracteristicas que se desean y una guia del arbol de proyecto, la documentacion inicial es vital para poner **DE MANERA EXPLICITA
LO QUE SE VA A HACER Y EL OBJETIVO GENERAL DEL PROYECTO, ASI COMO LOS OBJETIVOS ESPECIFICOS**

```
Explicit is better than implicit.
Simple is better than complex.
```
_zen de python_

```
Cuando una orden no se cumple, y la orden no es clara, la culpa es del general,
cuando la orden no se cumple y es muy clara, la culpa es del oficial
```
_Sung Tsu, El arte de la guerra_


## 4. FLUJOS DE TRABAJO CON GIT

los flujos de trabajo son formas en las que podemos organizar el trabajo de un
proyecto, bien sea que hagamos solos o con otras personas

### 4.1. TIPOS

hay muchos flujos de trabajo genericos, algunos son buenos para unas situaciones, otros para otras y depende
de la situacion lo mejor es hacer hibridos. los más genericos son:

* centralizado
* rama por caracteristica (branch per feature)
* basado en copias (fork)

### 4.2. EMPRESA

dependiendo de la cultura de los desarrolladores, en este caso, la latinoamericana, para el trabajo en
empresa lo mejor es optar por la arquitectura de rama por caracterstica, existen dos escenarios en los
que esta estrategia debe adaptarse

### 4.2.1. DESARROLLO

para desarrollo es bueno el de rama por caracteristica siendo el desarrollador lider quien siempre
realize la union de las ramas al finalizar el proceso

### 4.2.2. DESPLIEGUE

para el despliegue a servidor, la recomendacion es usar un bare repository, este es un repositorio de solo lectura
en el cual se puede navegar entre tags y branches pero no guardar cambios.

a veces hay pruebas o configuraciones que uno solo puede hacer en servidor, para ello hay que en lo posible
tener un servidor de pruebas diferente del de produccion

otra cosa que se puede lograr es hacer integracion continua (despliegue automatizado) usando git
como fuente para servicios como [jenkins](https://jenkins-ci.org/)

### 4.2.3. INCIDENCIAS (ISSUES)

para evitar el "fallo esto", "se cayo la plataforma en ..." la idea es usar un seguidor de incidencias
para que los clientes internos o desarrolladores le comenten al equipo de desarrollo los problemas y
sugerencias de mejora de una manera que la resolucion a los mismos pueda socializarse y rastrearse

### 4.3. OPENSOURCE

en el mundo opensource las reglas suelen ser diferentes, lo mejor es el trabajo por fork, un fork es una
copia completa de un repositorio la cual esta totalmente desligada del original, esto permite la mayor
flexibilidad posible para que los desarrolladores puedan trabajar a su gusto y enviar al repositorio comun
solamente el trabajo terminado, esto ayuda tambien al repositorio comun a tener solo versiones release de trabajo de multiples desarrolladores haciendo que el historial del repositorio sea mas limpio

### 4.4. DOCUMENTACION

#### markdown

markdown es una forma rapida de escribir documentacion sin preocuparse gran cosa por el estilo, tambien
nos ayuda a tener un formato que puede exportarse a multiples formatos

depende de la implementación del markdown este tiene algunas caracteristicas y no otras,
este documento esta **escrito totalmente en markdown**

#### manejo de documentacion

algunas personas prefieren no mezclar codigo y documentacion, tienen una wiki especificamente para documentar
y el codigo con cosas muy basicas.

personalmente prefiero tener documentacion y codigo juntos porque muchos desarrolladores "tienden a" tener pereza
de leerla, entonces es mejor ponerla muy a la vista

### que poner?

algunas cosas que debería tener una buena documentacion inicial (depende del proyecto)

* diagramas uml
* lista de caracteristicas actuales y planeadas
* equipo de desarrollo y contacto
* descripicion de carpetas y archivos general
* consideraciones especiales para montar entorno de desarrollo o de despliegue

## 4. PROYECTO CONJUNTO: NEWS SIDEBAR

NEWSBTC latinoamerica tiene un widget que muestra sus noticias de tal manera que cualquier persona puede embeberlas en su pagina web como podemos ver en [esta página](http://reikired.emdyp.me/#newsbtcEmdypWidget)

el proyecto se encuentra en este repositorio en la carpeta ```newsbtc_reader``` y la documentacion puede
[leerse aqui](git_workflow_files/newsbtc_reader/readme.md)

la mision en este taller:

1. clonar el repositorio como un fork
2. hacer una nueva hoja de estilos de tal manera que estas sean diferentes "temas" del widget, puede hacerse cambiando algunos colores, poniendo bordes, imagenes de fondo o cosas similares con css
3. enviar el cambio al servidor original
4. ver como el cambio es aceptado en el servidor original
5. mirar la historia del proyecto
