"""
flexible data structure generation

there are two kind of equipments being simulated, estacion that represents
a wheather monitoring station, and calidad_agua that represents an equipment
used to monitor water quality on industrial water incomes and dumps.

network generates a data stream that might change its structure depending
on the equipments connected, also variables in an equipment can change (pending
to implementation)
"""

from random import randrange
from datetime import datetime


class equipment(object):
    """
    represents a measurement equipment
    """

    def __init__(self, equipment_id):
        self._id = equipment_id
        self.max_value = 200
        self.span = 100
        self.variables = ['pressure', 'temperature']
        self.state = True

    def simulate(self):
        """
        generate one measurement of an equipment
        """
        # if turn on, create data, otherwise, just return None
        if self.state:
            data = {
            'timestamp': str(datetime.utcnow()),
            'equipment_id': self._id,
            }
            for v in self.variables:
                data[v] = randrange(0, self.max_value * int(self.span)) / self.span
            return data
        else:
            return None


class estacion(equipment):

    def __init__(self, equipment_id):
        equipment.__init__(self, equipment_id)
        self.variables = [
            'rain',
            'wind_direction',
            'soil_temperature',
            'environment_temperature',
            'humidity'
            ]


class calidad_agua(equipment):

    def __init__(self, equipment_id):
        equipment.__init__(self, equipment_id)
        self.variables = [
            'ph',
            'turbidity',
            'oil_in_water',
            'water_temperature',
        ]


class network(object):

    def __init__(self, network_id, equipments=None):
        self.equipments = equipments
        self._id = network_id

    def switch_equipment(self, equipment_id, state):
        """
        turn on or turn off an equipment in network by giving its id
        and the desired new state as boolean (True for on, False for off)
        """
        equipment = [
            a for a in self.equipments
            if self.equipments[a]._id == equipment_id
            ][0]
        self.equipments[equipment].state = state

    def simulate(self):
        """
        return a single measurement data
        """
        data = []
        for e in self.equipments:
            measure = self.equipments[e].simulate()
            if measure:
                data.append(measure)
        package = {
            'network': self._id,
            'tx_timestamp': str(datetime.utcnow()),
            'data': data
        }
        return package


def main():
    points = 100
    data = []
    # create network and its components
    equipos = {
        'ambiente1': estacion('001'),
        'ambiente2': estacion('002'),
        'vertimiento1': calidad_agua('003'),
        'entrada': calidad_agua('004')
    }
    red = network('G205', equipments=equipos)
    # network starts working
    for i in range(points):
        data = red.simulate()
    # equipment 001 is stoped for mantainance
    red.switch_equipment('001', False)
    # network continues working
    for i in range(points):
        data.append(red.simulate())
    # show or save data (data storage pending)
    print(data)


if __name__ == '__main__':
    main()
