# Python, el 101 de los lenguajes de programacion

## 1. Introduccion

![de_hacer_python](101_python_files/toca_scripting_python.jpg)

python es conocido sobre todo por ser el lenguaje perfecto para aprender a programar y por ser uno
de los preferidos para hacer scripting (código compacto que se ejecuta para tareas especificas), sin
embargo ofrece una gran cantidad de posibilidades

### zen de python

python tiene una filosofia (un estilo, una forma de ser) el cual se basa en _simplicidad_ y _explicitud_
puede verse escribiendo en la terminal de python ```import this```

```
The Zen of Python, by Tim Peters

Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
Special cases aren't special enough to break the rules.
Although practicality beats purity.
Errors should never pass silently.
Unless explicitly silenced.
In the face of ambiguity, refuse the temptation to guess.
There should be one-- and preferably only one --obvious way to do it.
Although that way may not be obvious at first unless you're Dutch.
Now is better than never.
Although never is often better than *right* now.
If the implementation is hard to explain, it's a bad idea.
If the implementation is easy to explain, it may be a good idea.
Namespaces are one honking great idea -- let's do more of those!

```

claves:

* idiomatico
* flexible
* legible
* mantenible
* modular

## 2. donde puedo correr python?

en casi cualquier dispositivo, corre en cualquier sistema operativo unix-like (unix, bsd, linux) en cualquier
arquitectura, en windows en la mayoria de las versiones, en mac-ios, en android... incluso en navegadores
web a traves de [brython](http://brython.info/index.html?lang=es), [usar brython en consola web](http://brython.info/tests/console.html?lang=es)

## 3. ventajas y desventajas

### 3.1. ventajas

* multiplataforma
* compatibilidad y extensibilidad
* tipado dinamico
* mayor velocidad de desarrollo
* adaptable a muchos paradigmas (POO, funcional, eventos...)
* gestor de paquetes (pip)
* comunidad activa desarrolladores

### 3.2. desventajas

* más lento que los lenguajes compilados

## 4. como empezar con python

### instalacion

la instalacion de python es muy sencilla, en los sistemas linux python viene instalado por defecto (las versiones 2.7) mientras que para windows y mac es comun tener que hacer doble-click en un instalador.

#### windows

instalar python es descargar el instalador y dar [doble-click como se muestra en esta guia](http://tutorial.djangogirls.org/es/python_installation/index.html)
el unico error que podría pasar es necesitar configurar una variable del sistema operativo llamada PYTHON_PATH,
error que puedes [corregir siguiendo estos pasos](http://stackoverflow.com/questions/3701646/how-to-add-to-the-pythonpath-in-windows-7)

python suele tener dos versiones diferenciadas, las 2.X y las 3.X, para saber en cual trabajar considera lo siguiente:

* si es un proyecto nuevo cuyas librerias son compatibles con python3, usa python3
* si es un proyecto antiguo con librerias solo para python2 o uno nuevo con librerias solo para python2, usa python2

para empezar, lo mejor es empezar trabajando con un IDE o entorno de desarrollo, para python los recomendados son:

* [ninja-ide](http://ninja-ide.org/) es exclusivo para python con muchas funciones para desarrollo web
* [spyder](https://github.com/spyder-ide/spyder) es sencillo de usar y tiene una interfaz similar a matlab

### lenguaje

hay que resaltar que la definicion de las variables es dinamica, y que todo en python es un objeto, por lo tanto
tiene propiedades (caracteristicas) y metodos (cosas que puede hacer sobre si mismo o sobre otros objetos)

```
# esto es un comentario de una linea

""" esto es un comenario
de multiples lineas, llamado docstring"""

import re  # importar una libreria
datetime import datetime  # importar un solo objeto de una libreria

entero = 2  # definir un numero entero
flotante = 3.3  # definir un numero flotante
palabra = 'hola'  # definir un string o linea de caracteres
verdadero = True  # definir un booleano
nada = None  # asi se definen los caracteres nulos o no existentes

lista = [1, 2, 3, 4, 5, 6]  # define un vector, en python llamado lista
lista[0] # obtener un valor de una lista, en este caso, el valor en la posicion 0
matriz = [  # una matriz de dos dimensiones seria una lista anidada en otra lista
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    ]
matriz[1][2]  # obtener el elemento en la posicion 2 de la lista de la posicion 1 (retorna 6)
diccionario = {'oso': 'bear', 'two': 2}  # un diccionario asocia un string o un numero con otro valor en parejas
diccionario['oso']  # retorna el valor de 'oso', en este caso 'bear'
tupla = (1, 2, 3)  # una tupla se parece a una lista, a exepcion de que la tupla tiene valores constantes

a = 2
b = 5
x = 'a'
y = 'b'

a + b  # suma de dos numeros
x + y  # concatenacion de dos strings
x * b  # repetir la cadena x el numero de veces de b
(a + b) *b  # agrupacion de terminos

frase = 'hola a todos'
len(frase)  # cuantos caracteres tene el string frase
frase.upper()  # pasar los caracteres de frase a mayusculas
frase.split(' ')  # separar la variable frase cada vez que se encuentre un caracter vacio
```

### pip

python index packages es un gestor de librerias, lo que significa que es un sitio donde puedes encontrar
librerias de python que hacen otros desarrolladores las cuales puedes descargar e instalar automaticamente
solo escribiendo:

```
pip install nombre_de_libreria
```

tambien las puedes actualizar, eliminar y empaquetar para poder estar seguro de que usas las mismas librerias
de un proyecto sin importar donde trabajes

### entornos virtuales

un entorno virtual es "una copia totalmente aislada de tu pc" en la cual puedes instalar las librerias que quieras
y  hacer los cambios que desees sin que esto afecte al python "por defecto", estos entornos virtuales pueden
transladarse de un equipo a otro de tal manera que las configuraciones locales de la maquina no alteran para nada
la ejecución del programa, sirven para:

* llevar una lista de configuraciones y librerias de un proyecto
* poder ejecutar el proyecto en multiples equipos en diferentes sistemas operativos sin ningun cambio

se usa la libreria _virtualenv_ la cual crea el entorno virtual, muchos programadores tambien la combinan con
la libreria _virtualenvwrapper_ la cual presta funciones adicionales como la gestion automatizada de los ejecutables
y entornos

se instalan con:

```
pip install virtualenv virtualenvwrapper
```

### extensiones en c++

python esta escrito en c++ de tal manera que cualquier libreria de c++ puede ser migrada a python con algunos
ajustes, estos ajustes se llaman _bindings_, a continuacion ponemos algunos de los más famosos

* pyside (para interfaces gráficas en qt)
* pygtk (para interfaces graficas en gtk)
* opencv python (procesamiento digital de imagenes)

## 5. ejemplos de python trabajando

en python se pueden hacer en unas cuantas lineas cosas que de otras maneras serían muy largas de hacer, a continuacion
algunos ejemplos

### monta un servidor

un corto ejemplo con tornado. [ver codigo](101_python_files/tornado_minimal_example.py)

```
import tornado.ioloop
import tornado.web

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello, world")

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
```

### haz web scrapping

un pequeño ejemplo de una libreria llamada requests que hace peticiones web y de beautifulsoap4 que procesa html.
[ver codigo aqui](101_python_files/mini_scrapper.py)

```
from bs4 import BeautifulSoup
from urllib.request import urlretrieve, urlopen
import threading
import time
import requests


class publication():
    """defines a publication, given the url gets all info required to do
    social network publication of a newsbtc new or event. Also can build
    any required custom-build publication on social networks by organize
    the required information"""
    title = ''
    url = ''
    image_link = ''

    def __init__(self, url, aditional='', mode='news', **kwargs):
        """class builder, always requires a url of the article, event or
        a custom valid url, also can require keyword arguments like
        aditional for aditional plain text to include in publication,
        mode that can be 'news' for a valid newsbtc url, 'event' for a valid
        newsbtc event url and 'custom' for custom-build publications,
        or data that recieves a dictionary used for custom-build publications,
        data must contain text, image(given in a url) and a description.

        This class define all data required to publish on
        facebook and twitter"""
        self.url = url
        #aditional publication info
        self.aditional = aditional
        #get the webpage
        try:
            text = urlopen(self.url, timeout=10).read()
        except:
            text = requests.get(self.url).text
        #parse html with default parser
        soup = BeautifulSoup(text, "html.parser")
        if mode in ['news', 'event']:
            #get post name
            self.title = str(soup.find('h1', class_='entry-title').string)
            if mode == 'event':
                self.title = '#eventNewsbtcLTA ' + self.title
            #get image url
            cl = 'attachment-post-thumbnail wp-post-image'
            img = soup.find_all('img', class_=cl)
            self.image_url = img[0]['src']
            #get image
            img_name, headers = urlretrieve(self.image_url)
            self.image_link = img_name
            #get description
            desc = soup.find_all('meta', property='og:description')
            self.description = desc[0]['content']
            #get author
            try:
                self.author = str(soup.find_all('h3',
                    class_='author-title')[0].string)
            except:
                self.author = str(soup.find_all('a',
                    class_='url fn n')[0].string)
        elif mode == 'custom':
            data = kwargs['data']
            #get post name
            self.title = data['text']
            #get image url
            self.image_url = data['image']
            #get image
            img_name, headers = urlretrieve(self.image_url)
            self.image_link = img_name
            #get description
            self.description = data['description']
            #get author
            self.author = 'NewsBTC LTA'

    def getData(self):
        """get data of publication in a dictionary, keywords are:
            title, url, image_link, image_url, description and author"""
        return {'title': self.title,
                'url': self.url,
                'image_link': self.image_link,
                'image_url': self.image_url,
                'description': self.description,
                'author': self.author}

if __name__ == '__main__':
    p = publication(
        'http://lta.newsbtc.com/2015/12/17/colombianos-hora-de-ahorrar-en-bitcoins/'
        )
    data = p.getData()
    print(data)
```

### reconocimiento digital de imagenes

una sencilla aplicacion de color falso, cortesia del ing Carlos Campo
[ver el codigo aqui](101_python_files/false_color.py)

```
import numpy as np
from cv2 import *


cap = VideoCapture(0)

TablaV = np.linspace(1, 255, 255)
TablaR = np.zeros(256)
TablaG = np.zeros(256)
TablaB = np.zeros(256)

recta = np.uint8(3 * TablaV[0: 84])

TablaR[0: 84] = recta
TablaR[85: 255] = 0

TablaG[0: 84] = 0
TablaG[85: 169] = recta
TablaG[170: 254] = 0

TablaB[0: 169] = 0
TablaB[170: 254] = recta


while (True):
    next, frame = cap.read()

    gray = cvtColor(frame, COLOR_BGR2GRAY)
    ima = cvtColor(frame, 1)

    imaR = TablaR[gray - 1]
    imaG = TablaG[gray - 1]
    imaB = TablaB[gray - 1]

    ima[:, :, 0] = imaB
    ima[:, :, 1] = imaG
    ima[:, :, 2] = imaR

    imshow("WebCam", ima)
    if waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
destroyAllWindows()

```

## 6. recursos para aprender más

* [codeacademy python](https://www.codecademy.com/learn/python) un curso paso a paso para aprender
* [check.io](https://www.checkio.org/) explora un mundo de fantasia pasando pruebas de creacion de programas, aprende buenas practicas y mira las soluciones a problemas comunes que dan otros programadores
* [documentacion oficial python](https://docs.python.org/3/) es bueno echar una leida para ver que cosas puede hacer el lenguaje por defecto sin necesidad de librerias extra
* [python package index - centro de librerias de python](https://pypi.python.org/pypi) puedes encontrar un monton de cosas utiles, Dont Repeat Yourself
